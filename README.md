# BagUp
BagUp merupakan aplikasi berbasis website yang digunakan oleh customer untuk memesan secara online. Melalui BagUp customer dapat melihat daftar katalog totebag yang disediakan, dan juga customer dapat melakukan pemesanan custom totebag yang diinginkan. Apabila customer ingin melakukan custom maka sistem akan meminta customer untuk menginputkan detail pesanan yang memuat informasi tentang nama, jumlah, warna, bahan, ukuran, dan desain.

## Pengembangan ini dipelopori oleh:

**Tim Bag Up**
- [X] Niluh Sekar Zulfa Umardhani / 2000016131 
- [X] Weni Sepriani / 2000016134 
- [X] Wahyu hidayat / 2000016025
- [X] Gilang damar maulana / 2000016018

## Milestone Stamp

- [X] 1 | Requirements Analysis
- [X] 2 | Sistem And Software Design
- [ ] 3 | Implementation
- [ ] 4 | Integration and System Testing
- [ ] 5 | Operation & Maintenance

## Melakukan Cloning
cd existing_repo

git clone https://gitlab.com/wenispr002702/bagup.git

## Status Proyek
On Going 


